import useHttp, { SYMBOL as HTTP } from "./useHttp";
import useAuth, { SYMBOL as AUTH } from "./useAuth";
import useTopics, { SYMBOL as TOPICS } from "./useTopics";
import { inject, provide } from "vue";
import { requireNotNull } from "@/utils/objects";

function providerFactory<T extends keyof Dependencies>(
  key: T,
  factory: () => Injectable<T>
) {
  const result = factory();
  provide(key, result);
  return result;
}

const dependencies = {
  [AUTH]: useAuth,
  [HTTP]: useHttp,
  [TOPICS]: useTopics,
};

type Dependencies = typeof dependencies;
type Injectable<T extends keyof Dependencies> = ReturnType<Dependencies[T]>;

export default class Injectables {
  public static http: typeof HTTP = HTTP;
  public static auth: typeof AUTH = AUTH;
  public static topics: typeof TOPICS = TOPICS;

  public static inject<T extends keyof Dependencies>(key: T): Injectable<T> {
    const dep = dependencies[key] as () => Injectable<T>;
    const provider = providerFactory(key, dep);
    const injectable = inject(key, provider, true);
    return requireNotNull(injectable, `Failed to inject '${key.toString()}'!`);
  }
}
