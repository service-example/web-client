import { ref, watch } from "vue";
import { useWebSocket, WebSocketResult } from "@vueuse/core";
import { getToken, Status, Token } from "./useAuth";
import {
  BehaviorSubject,
  distinct,
  filter,
  Observable,
  Subject,
  Subscription,
  tap,
} from "rxjs";
import { requireNotNull } from "@/utils/objects";
import useEnv from "@/composables/useEnv";
import Injectables from ".";

export const defaults = {
  baseUri: undefined,
  path: undefined,
};

export type TopicsOptions = {
  baseUri?: string;
  path?: string;
  token: Token;
};

export type File = {
  id: string;
  description: string;
  content: string;
};

export type Person = {
  id: string;
  name: string;
};

export type TopicMap = {
  file: File;
  person: Person;
};

export type Topic = keyof TopicMap;

export type Topics = {
  [K in Topic]: Map<string, TopicMap[K]>;
};

export type Topics$ = {
  [K in Topic]: BehaviorSubject<KafkaRecord<K> | null>;
};

export type RecordHandler<T extends Topic> = (
  record: KafkaRecord<T>
) => unknown;

export type KafkaRecord<T extends Topic = Topic> = {
  topic: T;
  timestamp: number;
  type: "CREATE" | "UPDATE" | "DELETE" | "POLL";
  data: TopicMap[T];
};

function statusFactory() {
  const subject = new BehaviorSubject(Status.NOT_INITIALIZED);

  return {
    subject$: subject,
    obs$: subject.pipe(distinct()),
  };
}

const status = statusFactory();

function createTopicsLogger() {
  status.obs$.subscribe({
    next: (status) => console.debug("Topics status: ", status),
    error: (e) => console.debug("Topics Failed: ", e),
  });
}

createTopicsLogger();

function init(options: Partial<TopicsOptions>) {
  let connection: WebSocketResult<KafkaRecord> | undefined = undefined;
  let subscription: Subscription | undefined = undefined;

  const records$ = new Subject<KafkaRecord>();

  const file$ = new BehaviorSubject(null);
  const person$ = new BehaviorSubject(null);

  const topics = { file$, person$ };

  return {
    status$: status.subject$,
    records$,
    topics,

    handleRecord<T extends Topic>(topic: T, handler: RecordHandler<T>) {
      records$
        .pipe(
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          filter((record) => record && record.topic === topic),
          tap(handler)
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        ) // @ts-ignore
        .subscribe(topics[`${topic}$`]);
    },

    publishRecord<T extends Topic>(topic: T, data: TopicMap[T]) {
      records$.next({
        data,
        topic,
        timestamp: new Date().getTime(),
        type: "POLL",
      });
    },

    subscribe(): Promise<Subscription | undefined> {
      if (status.subject$.value !== Status.NOT_INITIALIZED) {
        return Promise.resolve(subscription);
      }

      if (connection && subscription) {
        console.debug("Connection already established");
        return Promise.resolve(subscription);
      }

      status.subject$.next(Status.PENDING);
      console.debug("Init topics websocket endpoint...");

      const token = getToken(options);
      const uri = `${options.baseUri}${options.path}?access_token=${token}`;

      connection = useWebSocket(uri, {
        autoReconnect: true,
        immediate: false,
      });

      const ws$ = new Observable<KafkaRecord>((subscriber) => {
        const conn = requireNotNull(connection, "Connection not established!");

        watch(conn.ws, (ws) => {
          ws?.addEventListener("open", () =>
            status.subject$.next(Status.SUCCESS)
          );

          ws?.addEventListener("message", (message: MessageEvent) => {
            if (status.subject$.value === Status.PENDING) {
              status.subject$.next(Status.SUCCESS);
            }

            try {
              const record = JSON.parse(message.data);
              subscriber.next(record);
            } catch (e) {
              subscriber.error(e);
            }
          });

          ws?.addEventListener("error", (error: Event) => {
            return subscriber.error(new Error(error.type));
          });

          ws?.addEventListener("close", () => {
            status.subject$.next(Status.FAILED);
            return subscriber.complete();
          });
        });
      });

      subscription = ws$
        .pipe(tap((record) => console.debug("Record received: ", record)))
        .subscribe({
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          next: (record) => records$.next(record),
          error: (e) => console.error(e),
          complete: () => console.warn("Topics subscription ended"),
        });

      connection.open();

      return new Promise((resolve, reject) => {
        status.obs$.subscribe({
          next: (status) => {
            if (status === Status.SUCCESS) {
              resolve(subscription);
            }
          },
          error: reject,
        });
      });
    },
  };
}

export function topicOptionsFactory(
  options: Partial<TopicsOptions> = {}
): Partial<TopicsOptions> {
  return { ...defaults, ...options };
}

export const SYMBOL = Symbol("topics");
let authSub: Subscription | undefined;
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function useTopics() {
  const { wsUri } = useEnv();
  const { token, status$: auth$ } = Injectables.inject(Injectables.auth);
  const options = {
    token,
    baseUri: wsUri.toString(),
    path: "topics",
  };

  const {
    topics: observables,
    subscribe,
    handleRecord,
    publishRecord,
  } = init(options);

  const file = ref(new Map<string, File>());
  const person = ref(new Map<string, Person>());
  const topics = { file, person };

  Object.entries(topics).forEach(([topic, records]) => {
    handleRecord(topic as Topic, ({ data, type }) => {
      switch (type) {
        case "DELETE":
          return records.value.delete(data.id);
        default:
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          return records.value.set(data.id, data as any);
      }
    });
  });

  watch(topics.file, console.log);

  const result = {
    subscribe,
    publishRecord,
    handleRecord,

    ...observables,
    topics,
  };

  if (!authSub) {
    authSub = auth$.subscribe(async (auth) => {
      if (
        auth === Status.SUCCESS &&
        (status.subject$.value === Status.NOT_INITIALIZED ||
          status.subject$.value === Status.FAILED)
      ) {
        await subscribe();
      }
    });
  }

  return result;
}
