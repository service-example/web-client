import merge from "lodash/merge";
import axios, { AxiosInstance, AxiosRequestHeaders } from "axios";
import useAuth, { getToken } from "./useAuth";

export const defaults = {
  baseUrl: undefined,
  token: undefined,
  headers: {
    "Content-Type": "application/json",
  },
};

export function transformRequestFactory() {
  return (data: unknown, headers?: AxiosRequestHeaders): unknown => {
    const token = getToken(useAuth());

    if (headers && token) headers.Authorization ??= "Bearer " + token;
    return JSON.stringify(data);
  };
}

export function headerFactory(
  headers: Partial<AxiosRequestHeaders> = {}
): AxiosRequestHeaders {
  return merge(defaults.headers, headers) as AxiosRequestHeaders;
}

export type HttpOptions = {
  baseUrl?: string;
  headers: Partial<AxiosRequestHeaders>;
};

export type HttpResult = {
  http: AxiosInstance;
};

export const SYMBOL = Symbol("http");

export default function useHttp(
  options: Partial<HttpOptions> = defaults
): HttpResult {
  const result = {
    http: axios.create({
      headers: headerFactory(options.headers),
      baseURL: "http://localhost:8080/api/v1",
      transformRequest: transformRequestFactory(),
    }),
  };

  return result;
}
