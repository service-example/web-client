import type {
  KeycloakConfig,
  KeycloakInitOptions,
  KeycloakInstance,
  KeycloakTokenParsed,
} from "keycloak-js";
import type { ComputedRef, Ref } from "vue";

import Keycloak from "keycloak-js";
import merge from "lodash/merge";
import { computed } from "vue";
import useEnv from "@/composables/useEnv";
import { BehaviorSubject } from "rxjs";
import { useLocalStorage } from "@vueuse/core";

type AuthOptions<Init = KeycloakInitOptions> = KeycloakConfig & {
  init: Init;
};

const defaults: Partial<AuthOptions> = {
  url: undefined,
  realm: undefined,
  clientId: undefined,
  init: {
    flow: "standard",
    redirectUri: undefined,
    onLoad: "login-required",
    checkLoginIframe: false,
  },
};

export type Token = string | Ref<string | undefined> | undefined;

export function getToken(options: { token?: Token }): string | undefined {
  return typeof options.token === "string"
    ? options.token
    : options.token?.value;
}

export function configFactory(options: PartialAuthOptions = {}): AuthOptions {
  return merge(defaults, options) as AuthOptions;
}

type PartialAuthOptions = Partial<AuthOptions<Partial<KeycloakInitOptions>>>;

export type AuthenticationResult = {
  status$: BehaviorSubject<Status>;

  keycloak: KeycloakInstance;

  token: Ref<string | undefined>;
  tokenParsed: ComputedRef<KeycloakTokenParsed | undefined>;
  authenticated: boolean;

  init(): Promise<void>;
  authenticate(): Promise<boolean>;
  refresh(): Promise<void>;
};

export const SYMBOL = Symbol("auth");

export const enum Status {
  NOT_INITIALIZED = "NOT_INITIALIZED",
  PENDING = "PENDING",
  SUCCESS = "SUCCESS",
  FAILED = "FAILED",
}

const status$ = new BehaviorSubject<Status>(Status.NOT_INITIALIZED);

function createAuthLogger() {
  status$.subscribe({
    next: (status) => console.debug("Authentication status: ", status),
    error: (e) => console.debug("Authentication Failed: ", e),
  });
}

const { baseUrl, realm, clientId } = useEnv();
const options = {
  url: `${baseUrl}auth`,
  realm,
  clientId,
  init: {
    redirectUri: `${baseUrl.toString()}redirect?`,
  },
};
const config = configFactory(options);
const keycloak = Keycloak(config);

export default function useAuth(): AuthenticationResult {
  const token = useLocalStorage<string | undefined>("token", undefined);
  const tokenParsed = computed(() => keycloak?.tokenParsed);

  const auth = {
    status$,

    keycloak,
    token,
    tokenParsed,

    async init(): Promise<void> {
      createAuthLogger();

      if (keycloak && !keycloak.authenticated && !status) {
        await auth.authenticate.call(auth);
      }
    },

    async authenticate(): Promise<boolean> {
      console.debug("Authenticate...");

      try {
        const origin = new URL(window.location.href).pathname;
        const query = origin ? new URLSearchParams({ origin }).toString() : "";

        const success = await keycloak.init({
          ...config.init,
          redirectUri: `${baseUrl.toString()}redirect?${query}`,
          token: localStorage.getItem("token") ?? undefined,
        });

        token.value = keycloak.token;
        status$.next(success ? Status.SUCCESS : Status.FAILED);
        return success;
      } catch (e) {
        status$.error(e);
        return false;
      }
    },

    async refresh(): Promise<void> {
      console.debug("Refresh...");

      // Token Refresh
      setInterval(async () => {
        try {
          const refreshed = await keycloak.updateToken(70);
          if (refreshed) {
            console.info("Token refreshed" + refreshed);
          } else if (keycloak) {
            const { tokenParsed, timeSkew } = keycloak;
            const valid = Math.round(
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              tokenParsed.exp + timeSkew - new Date().getTime() / 1000
            );

            console.warn("Token not refreshed, valid for ", valid + " seconds");
          }
        } catch (e) {
          console.error("Failed to refresh token");
        }
      }, 6000);
    },

    get authenticated(): boolean {
      return (!!keycloak && keycloak.authenticated) || false;
    },
  };

  if (
    status$.value === Status.NOT_INITIALIZED ||
    status$.value === Status.FAILED
  ) {
    auth.init.call(auth);
  }

  return auth;
}
