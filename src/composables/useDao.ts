import type { Topic, TopicMap } from "../injectables/useTopics";
import { computed, ComputedRef, ref, Ref } from "vue";

import Injectables from "../injectables/index";
import { omit } from "lodash";

export const enum EditMode {
  // NONE = 0,
  CREATE = "create",
  UPDATE = "update",
  // DELETE = 3,
}

export type Data<T extends Topic> = TopicMap[T];

export type Dao<T extends Topic> = {
  model: Ref<Data<T> | Omit<Data<T>, "id">>;
  mode: Ref<EditMode>;
  objects: ComputedRef<Data<T>[]>;

  getAll(): Promise<Data<T>[]>;
  save(): Promise<Data<T>>;
  update(): Promise<Data<T>>;
  remove(id: string): Promise<void>;
};

export type Factory<T extends Topic> = () => Omit<Data<T>, "id">;

export type DaoOptions<T extends Topic> = {
  topic: T;
  factory: Factory<T>;
};

export default function useDao<T extends Topic>(
  options: DaoOptions<T>
): Dao<T> {
  const { topic, factory } = options;
  const { http } = Injectables.inject(Injectables.http);
  const { topics, publishRecord } = Injectables.inject(Injectables.topics);

  const model = ref(factory()) as Ref<Data<T>>;
  const mode = computed(() => {
    return model.value.id && topics[topic].value.has(model.value.id)
      ? EditMode.UPDATE
      : EditMode.CREATE;
  });
  const objects = computed(() => {
    return Array.from(topics[topic].value.values() as Iterable<Data<T>>);
  });

  return {
    model,
    mode,
    objects,

    async getAll(): Promise<Data<T>[]> {
      const { data, status } = await http.get<Data<T>[]>(topic);

      if (status >= 200 && status < 300) {
        data.forEach((file) => publishRecord(topic, file));
        return data;
      }

      return [];
    },

    async save(): Promise<Data<T>> {
      const object = omit(model.value, "id");
      console.log(`Save ${topic}: ${JSON.stringify(object)}`);

      const start = performance.now();
      const { status, data } = await http.post(`/${topic}`, object);

      if (status >= 200 && status < 300) {
        console.log(
          `${topic} saved successfully in ${performance.now() - start}ms`
        );
      } else {
        console.error(
          `Saving ${topic} failed after ${performance.now() - start}ms`
        );
      }

      if (data) {
        model.value.id = data.id;
      }

      return object as Data<T>;
    },

    async update(): Promise<Data<T>> {
      const object = model.value;
      console.log(`Edit ${topic}: ${JSON.stringify(object)}`);

      const start = performance.now();
      const { status } = await http.put(topic, object);

      if (status >= 200 && status < 300) {
        console.log(
          `${topic} updated successfully in ${performance.now() - start}ms`
        );
      } else {
        console.error(
          `Updating ${topic} failed after ${performance.now() - start}ms`
        );
      }

      return object;
    },

    async remove(id: string): Promise<void> {
      console.log(`delete ${topic}: ${id}`);

      const start = performance.now();
      const { status } = await http.delete(topic, { params: { id } });

      if (status >= 200 && status < 300) {
        console.log(
          `${topic} deleted successfully in ${performance.now() - start}ms`
        );
      } else {
        console.error(
          `Deleting ${topic} failed after ${performance.now() - start}ms`
        );
      }
    },
  };
}
