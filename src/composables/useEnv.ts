import kebabCase from "lodash/kebabCase";

export type Env = {
  // location
  baseUrl: URL;
  wsUri: URL;

  // auth
  realm: string;
  clientId: string;
};

function getBaseUrl(): URL {
  return new URL(new String(process.env.VUE_APP_BASE_URL).trim());
}

function getWebSocketUri(): URL {
  const uri = new URL(new String(process.env.VUE_APP_BASE_URL).trim());
  uri.protocol = "ws";
  return uri;
}

function getClientId(): string {
  return kebabCase(new String(process.env.VUE_APP_CLIENT_ID).trim());
}

function getRealm(): string {
  return kebabCase(new String(process.env.VUE_APP_REALM).trim());
}

export default function useEnv(): Env {
  return {
    baseUrl: getBaseUrl(),
    wsUri: getWebSocketUri(),

    realm: getRealm(),
    clientId: getClientId(),
  };
}
