import type { Topic } from "@/injectables/useTopics";
import { Dao, DaoOptions, Data } from "./useDao";

import Injectables from "@/injectables";
import useDao from "./useDao";
import { Status } from "@/injectables/useAuth";

export type TopicRouteResult<T extends Topic> = {
  dao: Dao<T>;

  select(object: Data<T>): void;
  discard(): void;
};

export default function useTopicRoute<T extends Topic>(
  options: DaoOptions<T>
): TopicRouteResult<T> {
  const { factory } = options;
  const { status$ } = Injectables.inject(Injectables.auth);

  const dao = useDao<T>(options);

  status$.subscribe(async (status) => {
    if (status === Status.SUCCESS) {
      await dao.getAll();
    }
  });

  return {
    dao,

    select(object: Data<T>) {
      dao.model.value = { ...object };
    },

    discard() {
      dao.model.value = factory();
    },
  };
}
