export function requireNotNull<T>(
  object: T,
  msg?: string
): Exclude<T, undefined | null> {
  if (
    object === undefined ||
    object === null ||
    typeof object === "undefined"
  ) {
    throw new Error(msg ?? "RequireNotNullError");
  }

  return object as Exclude<T, undefined | null>;
}
