import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import Files from "../views/Files.vue";
import Persons from "../views/Persons.vue";
import Redirect from "../views/Redirect.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/file",
    name: "Files",
    component: Files,
  },
  {
    path: "/person",
    name: "Persons",
    component: Persons,
  },
  {
    path: "/redirect",
    name: "LoginRedirect",
    component: Redirect,
  },
  {
    path: "/:pathMatch(.*)",
    redirect: "/",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
